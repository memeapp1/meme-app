import { vitePlugin as remix } from "@remix-run/dev";
import { installGlobals } from "@remix-run/node";
import { defineConfig } from "vite";
import tsconfigPaths from "vite-tsconfig-paths";
import Unimport from 'unimport/unplugin'

installGlobals();

export default defineConfig({
  plugins: [remix(), tsconfigPaths(),
  // https://dev.to/marcosviana/remix-with-automatic-importation-have-cleaner-code-50j2
  Unimport.vite({
    dts: true,
    dirs: [
      './app/components/*',
      '../../packages/ui/src/components/*'
    ],
    presets: [
      'react',
      {
        from: '@remix-run/react',
        imports: [
          'useLoaderData', 'useActionData', 'useLocation', 'useNavigation',
          'useNavigate', 'useParams', 'useAsyncError', 'useAsyncValue',
          'useBeforeUnload', 'useBlocker', 'useFetcher', 'useFetchers',
          'useFormAction', 'useHref', 'useMatches', 'useNavigationType',
          'useOutlet', 'useOutletContext', 'unstable_usePrompt', 'useResolvedPath',
          'useRevalidator', 'useRouteError', 'useRouteLoaderData', 'useSearchParams',
          'useSubmit', 'unstable_useViewTransitionState', 'Link', 'Form', 'Await', 'Links',
          'Outlet', 'NavLink', 'PrefetchPageLinks'
        ],
        priority: 10
      },
      {
        from: '@remix-run/node',
        imports: [
          'json', 'redirect', 'defer', 'createCookie', 'isRouteErrorResponse', 'redirectDocument',
          'createCookieSessionStorage', 'createMemorySessionStorage', 'createFileSessionStorage'
        ]
      },
      {
        from: '@remix-run/node',
        type: true,
        imports: [
          'ActionFunctionArgs', 'LoaderFunctionArgs',
          'MetaFunctionArgs', 'MetaFunction', 'ClientActionFunctionArgs',
          'ClientLoaderFunctionArgs', 'HeadersFunction', 'LinksFunction',
          'ShouldRevalidateFunction'
        ]
      }
    ]
  })
  ],
});
