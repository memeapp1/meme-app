#!/bin/bash

# Configure the Prisma PostgreSQL provider.
sed -i -e 's/provider = "sqlite"/provider = "postgresql"/' ./prisma/schema.prisma

# Uncomment the case-insensitive mode for the PostgreSQL provider.
# Reference: https://www.prisma.io/docs/orm/prisma-client/queries/case-sensitivity#postgresql-provider
sed -i -e "s/\/\/mode: 'insensitive'/mode: 'insensitive'/" ./app/models/comment.server.ts
sed -i -e "s/\/\/mode: 'insensitive'/mode: 'insensitive'/" ./app/models/meme.server.ts
sed -i -e "s/\/\/mode: 'insensitive'/mode: 'insensitive'/" ./app/models/user.server.ts