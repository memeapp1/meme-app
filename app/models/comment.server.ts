import { prisma } from "~/db.server";
import { Comment, Prisma } from "@prisma/client";

const commentRecordsPerPage = 10;

interface Dictionary<T> {
  [Key: string]: T;
}

export class ValidationFields {
  Fields: Dictionary<string> = {};
}

export type CommentResult = {
  id: string,
  text: string;
  memeSlug: string;
  moderationResult?: string;
  moderationComment?: string;
  active?: boolean;
  createdByUserId?: string;
  createdByUserName?: string;
  createdDate?: Date;
  updatedDate?: Date;
  validationFields?: ValidationFields;
};

export type CommentPosition = {
  rowNumber: BigInt;
}

export async function getComments(
  slug: string,
  userId?: string,
  includeUnapproved: boolean = false,
  page: number = 1) {

  if (userId == undefined) {
    userId = '';
  }

  if (page == null
    || page < 1) {
    page = 1;
  }

  const skip = (page - 1) * commentRecordsPerPage;
  const take = commentRecordsPerPage;

  const commentsTotalResultsCount = await prisma.comment.count({
    where: {
      OR: [
        {
          active: true,
          memeSlug: slug
        },
        {
          active: !includeUnapproved,
          memeSlug: slug
        },
        {
          createdByUserId: userId,
          memeSlug: slug
        }
      ],
    },
  });

  const commentsTotalPageCount = Math.ceil(commentsTotalResultsCount / commentRecordsPerPage);

  const comments = await prisma.$queryRaw<CommentResult[]>(
    Prisma.sql`SELECT c."id",
                      c."text",
                      c."moderationComment",
                      c."active",
                      c."memeSlug",
                      c."createdByUserId",
                      (SELECT u."username"
                         FROM "User" u
                        WHERE u."id" = c."createdByUserId"
                      ) AS "createdByUserName",
                      c."createdDate",
                      c."updatedDate"
                 FROM "Comment" c 
                WHERE c."memeSlug" = ${slug}
                  AND (c."active" = true OR c."active" = ${!includeUnapproved} OR c."createdByUserId" = ${userId})
                ORDER BY c."createdDate"
                LIMIT ${take} OFFSET ${skip}`
  );

  return { comments, commentsTotalPageCount, commentsTotalResultsCount };
}

export async function getAllModerationComments(
  search?: string,
  page: number = 1) {

  if (search == null) {
    search = '';
  }

  if (page == null
    || page < 1) {
    page = 1;
  }

  const skip = (page - 1) * commentRecordsPerPage;
  const take = commentRecordsPerPage;

  const totalResultsCount = await prisma.comment.count({
    where: {
      text: {
        contains: search,
        mode: 'insensitive'
      },
    },
  });

  const totalPageCount = Math.ceil(totalResultsCount / commentRecordsPerPage);

  const results = await prisma.comment.findMany({
    skip: skip,
    take: take,
    include: {
      meme: {
        select: {
          topText: true,
          bottomText: true
        }
      }
    },
    where: {
      OR: [
        {
          text: {
            contains: search,
            mode: 'insensitive'
          }
        },
        {
          meme:
          {
            topText: {
              contains: search,
              mode: 'insensitive'
            }
          }
        },
        {
          meme:
          {
            bottomText: {
              contains: search,
              mode: 'insensitive'
            }
          }
        }
      ]
    },
    orderBy: [
      {
        createdDate: 'desc',
      },
    ],
  });

  return { results, totalPageCount, totalResultsCount };
}

export async function getComment(id: string): Promise<CommentResult | undefined> {
  const comment = await prisma.$queryRaw<CommentResult[]>(
    Prisma.sql`SELECT c."id",
                      c."text",
                      c."memeSlug",
                      c."createdByUserId",
                      (SELECT u."username"
                         FROM "User" u
                        WHERE u."id" = c."createdByUserId"
                      ) AS "createdByUserName",
                      c."createdDate",
                      c."updatedDate"
                 FROM "Comment" c 
                WHERE c."id" = ${id}
                LIMIT 1`
  );

  if (comment.length > 0) {
    return comment[0];
  }
  else {
    return undefined;
  }
}

export async function getCommentPageNumber(
  id: string, 
  memeSlug: string,
  userId?: string,
  includeUnapproved: boolean = false,
): Promise<number | undefined> {

  if (userId == undefined) {
    userId = '';
  }

  const commentPosition = await prisma.$queryRaw<CommentPosition[]>(
    Prisma.sql`SELECT "rowNumber"
                 FROM
                  (
                     SELECT c."id",
                            ROW_NUMBER() OVER(ORDER BY c."createdDate") AS "rowNumber"
                       FROM "Comment" c 
                      WHERE c."memeSlug" = ${memeSlug}
                        AND (c."active" = true OR c."active" = ${!includeUnapproved} OR c."createdByUserId" = ${userId})
                  ) t
                 WHERE t."id" = ${id}`
  );

  if (commentPosition.length > 0) {
    const rowNumber = Number.parseInt(commentPosition[0].rowNumber.toString());
    const pageNumber = Math.ceil(rowNumber / commentRecordsPerPage);

    return pageNumber;
  }
  else {
    return undefined;
  }
}

export async function createComment(
  comment: Pick<Comment, "memeSlug" | "text" | "moderationResult" | "active" | "createdByUserId">
) {
  return prisma.comment.create({ data: comment });
}

export async function updateComment(
  comment: Pick<Comment, "id" | "text" | "moderationResult" | "active" | "updatedByUserId">
) {
  return prisma.comment.update({
    where: {
      id: comment.id
    },
    data: comment,
  })
}

export async function removeComment(
  id: string
) {
  return prisma.comment.delete({
    where: {
      id
    },
  })
}

type CommentModerationAction = {
  id: string;
  approve: boolean;
  moderationComment?: string
  updatedByUserId: string;
};

export async function updateCommentModerationAction(moderationAction: CommentModerationAction) {
  const meme = await getComment(moderationAction.id);
  if (!meme) {
    throw new Error(`No comment found for ${moderationAction.id}`);
  }

  return prisma.comment.update({
    where: {
      id: moderationAction.id,
    },
    data: {
      active: moderationAction.approve,
      moderationComment: moderationAction.approve == false
        ? moderationAction.moderationComment
        : null,
      updatedByUserId: moderationAction.updatedByUserId
    }
  })
}