import { prisma } from "~/db.server";
import { type Meme, type Vote } from "@prisma/client";

const memeRecordsPerPage = 10;

export type MemeWithUserVote = {
  slug: string;
  voted: boolean;
  voteCount: number;
};

export type MemeListItem = {
  slug: string,
  memeType: string,
  topText: string,
  bottomText: string,
  createdByUserId: string,
  hasUserVoted: boolean;
  voteCount: number;
};

export async function getMemes(
  userId?: string,
  search?: string,
  includeUnapproved: boolean = false,
  page: number = 1) {

  if (userId == undefined) {
    userId = '';
  }

  if (search == null) {
    search = '';
  }

  if (page == null
    || page < 1) {
    page = 1;
  }

  const skip = (page - 1) * memeRecordsPerPage;
  const take = memeRecordsPerPage;

  const totalResultsCount = await prisma.meme.count({
    where: {
      OR: [
        {
          active: true,
          topText: {
            contains: search,
            mode: 'insensitive'
          }
        },
        {
          active: !includeUnapproved,
          topText: {
            contains: search,
            mode: 'insensitive'
          }
        },
        {
          createdByUserId: userId,
          topText: {
            contains: search,
            mode: 'insensitive'
          }
        },

        {
          active: true,
          bottomText: {
            contains: search,
            mode: 'insensitive'
          }
        },
        {
          active: !includeUnapproved,
          bottomText: {
            contains: search,
            mode: 'insensitive'
          }
        },
        {
          createdByUserId: userId,
          bottomText: {
            contains: search,
            mode: 'insensitive'
          }
        }
      ],
    },
  });

  const totalPageCount = Math.ceil(totalResultsCount / memeRecordsPerPage);

  let orderBy = [
    {
      vote: { _count: 'desc' }
    },
    {
      createdDate: 'desc',
    },
  ];

  if (includeUnapproved) {
    orderBy = [
      {
        updatedDate: 'desc',
      },
      {
        createdDate: 'desc',
      },
    ];
  }

  const results = await prisma.meme.findMany({
    skip: skip,
    take: take,
    select: {
      id: true,
      slug: true,
      memeType: true,
      topText: true,
      bottomText: true,
      moderationResult: true,
      moderationComment: true,
      active: true,
      createdByUserId: true,
      createdDate: true,
      updatedByUserId: true,
      updatedDate: true,
      _count: {
        select: { vote: true },
      },
      vote: {
        where: {
          createdByUserId: userId,
        },
      },
    },
    where: {
      OR: [
        {
          active: true,
          topText: {
            contains: search,
            mode: 'insensitive'
          }
        },
        {
          active: !includeUnapproved,
          topText: {
            contains: search,
            mode: 'insensitive'
          }
        },
        {
          createdByUserId: userId,
          topText: {
            contains: search,
            mode: 'insensitive'
          }
        },

        {
          active: true,
          bottomText: {
            contains: search,
            mode: 'insensitive'
          }
        },
        {
          active: !includeUnapproved,
          bottomText: {
            contains: search,
            mode: 'insensitive'
          }
        },
        {
          createdByUserId: userId,
          bottomText: {
            contains: search,
            mode: 'insensitive'
          }
        }
      ],
    },
    orderBy: orderBy,
  });

  return { results, totalPageCount, totalResultsCount };
}

export async function getSimilarMemes(
  userId?: string,
  slug?: string,
  memeType?: string,
  page: number = 1) {

  if (userId == undefined) {
    userId = '';
  }

  if (page == null
    || page < 1) {
    page = 1;
  }

  const skip = (page - 1) * memeRecordsPerPage;
  const take = memeRecordsPerPage;

  const similarMemesTotalResultsCount = await prisma.meme.count({
    where: {
      memeType,
      NOT: {
        slug: {
          equals: slug
        },
      },
    },
  });

  const similarMemesTotalPageCount = Math.ceil(similarMemesTotalResultsCount / memeRecordsPerPage);

  const similarMemes = await prisma.meme.findMany({
    skip: skip,
    take: take,
    select: {
      id: true,
      slug: true,
      memeType: true,
      topText: true,
      bottomText: true,
      moderationResult: true,
      moderationComment: true,
      active: true,
      createdByUserId: true,
      createdDate: true,
      updatedByUserId: true,
      updatedDate: true,
      _count: {
        select: { vote: true },
      },
      vote: {
        where: {
          createdByUserId: userId,
        },
      },
    },
    where: {
      memeType,
      NOT: {
        slug: {
          equals: slug
        },
      },
    },
    orderBy: [
      {
        vote: { _count: 'desc' }
      },
      {
        createdDate: 'desc',
      },
    ],
  });

  return { similarMemes, similarMemesTotalPageCount, similarMemesTotalResultsCount };
}

export async function getMeme(slug: string, userId?: string) {

  if (userId == undefined) {
    userId = '';
  }

  return prisma.meme.findUnique({
    select: {
      id: true,
      slug: true,
      memeType: true,
      topText: true,
      bottomText: true,
      moderationResult: true,
      moderationComment: true,
      active: true,
      createdByUserId: true,
      createdDate: true,
      updatedByUserId: true,
      updatedDate: true,
      _count: {
        select: { vote: true },
      },
      vote: {
        where: {
          createdByUserId: userId,
        },
      },
    },
    where: { slug }
  });
}

export async function getMemeWithImageData(slug: string, userId?: string) {

  if (userId == undefined) {
    userId = '';
  }

  return prisma.meme.findUnique({
    select: {
      id: true,
      slug: true,
      memeType: true,
      topText: true,
      bottomText: true,
      imageData: true,
      moderationResult: true,
      moderationComment: true,
      active: true,
      createdByUserId: true,
      createdDate: true,
      updatedByUserId: true,
      updatedDate: true,
      _count: {
        select: { vote: true },
      },
      vote: {
        where: {
          createdByUserId: userId,
        },
      },
    },
    where: { slug }
  });
}

export async function getMemeById(id: string, userId?: string) {

  if (userId == undefined) {
    userId = '';
  }

  return prisma.meme.findUnique({
    select: {
      id: true,
      slug: true,
      memeType: true,
      topText: true,
      bottomText: true,
      moderationResult: true,
      moderationComment: true,
      active: true,
      createdByUserId: true,
      createdDate: true,
      updatedByUserId: true,
      updatedDate: true,
      _count: {
        select: { vote: true },
      },
      vote: {
        where: {
          createdByUserId: userId,
        },
      },
    },
    where: { id }
  });
}

export async function getUnrenderedMemeCount() {
  return await prisma.meme.count({
    where: {
      OR: [
        {
          imageData: null
        },
        {
          imageData: ''
        }
      ]
    },
  });
}

export async function getNextUnrenderedMeme() {
  const totalResultsCount = await prisma.meme.count({
    where: {
      OR: [
        {
          imageData: null
        },
        {
          imageData: ''
        },
        {
          isBrowserRender: false
        }
      ]
    },
  });

  const results = await prisma.meme.findMany({
    take: 1,
    where: {
      OR: [
        {
          imageData: null
        },
        {
          imageData: ''
        },
        {
          isBrowserRender: false
        }
      ]
    },
    orderBy: [
      {
        createdDate: 'desc',
      },
    ],
  });

  return { results, totalResultsCount };
}

export async function createMeme(
  meme: Pick<Meme, "slug" | "memeType" | "topText" | "bottomText" | "imageData" | "moderationResult" | "active" | "createdByUserId">
) {
  return prisma.meme.create({ data: meme });
}

export async function removeMeme(
  slug: string
) {
  return prisma.meme.delete({
    where: {
      slug
    },
  });
}

export async function addVote(
  vote: Pick<Vote, "memeSlug" | "createdByUserId">
) {
  return prisma.vote.create({ data: vote });
}

export async function removeVote(
  vote: Pick<Vote, "memeSlug" | "updatedByUserId">
) {
  return prisma.vote.deleteMany({
    where: {
      memeSlug: vote.memeSlug,
      createdByUserId: vote.updatedByUserId?.toString()
    }
  });
}

type MemeMutation = {
  slugId?: string;
  vote?: boolean;
  updatedByUserId: string;
};

export async function updateMeme(
  slug: string,
  meme: Pick<Meme, "slug" | "memeType" | "topText" | "bottomText" | "imageData" | "moderationResult" | "active" | "updatedByUserId">
) {
  return prisma.meme.update({
    where: {
      slug: slug,
    },
    data: meme,
  })
}

export async function updateMemeImageData(
  slug: string,
  imageData: string,
  isBrowserRender: boolean,
  updatedByUserId: string
) {
  return prisma.meme.update({
    where: {
      slug: slug,
    },
    data: {
      imageData,
      isBrowserRender,
      updatedByUserId
    },
  })
}

export async function updateMemeVote(slug: string, updates: MemeMutation) {
  const meme = await getMeme(slug, updates.updatedByUserId.toString());
  if (!meme) {
    throw new Error(`No meme found for ${slug}`);
  }

  if (updates.vote === true && meme.vote.length == 0) {
    await addVote({ memeSlug: slug, createdByUserId: updates.updatedByUserId.toString() })
  }
  else {
    await removeVote({ memeSlug: slug, updatedByUserId: updates.updatedByUserId.toString() })
  }

  return await getMeme(slug, updates.updatedByUserId.toString());
}

type MemeModerationAction = {
  slug: string;
  approve: boolean;
  moderationComment?: string
  updatedByUserId: string;
};

export async function updateMemeModerationAction(moderationAction: MemeModerationAction) {
  const meme = await getMeme(moderationAction.slug, moderationAction.updatedByUserId.toString());
  if (!meme) {
    throw new Error(`No meme found for ${moderationAction.slug}`);
  }

  return prisma.meme.update({
    where: {
      slug: moderationAction.slug,
    },
    data: {
      active: moderationAction.approve,
      moderationComment: moderationAction.approve == false
        ? moderationAction.moderationComment
        : null,
      updatedByUserId: moderationAction.updatedByUserId
    }
  })
}