import { getModerationResults } from "./content-moderation";
import { requireUser } from "./session.server";
import { COMMENT_MAX_CHARACTER_LENGTH, MIN_CHARACTER_LENGTH, checkIfEditable } from "./utils";
import invariant from "tiny-invariant";
import { createComment, getComment, removeComment, updateComment } from "./models/comment.server";

export const validateComment = (comment: string) => {
  if (typeof comment !== "string" || comment.length === 0) {
    return `Comment is required.`;
  }
  else if (comment.length < MIN_CHARACTER_LENGTH) {
    return `Comment must be at least ${MIN_CHARACTER_LENGTH} characters long.`;
  }
  else if (comment.length > COMMENT_MAX_CHARACTER_LENGTH) {
    return `Comment must be no more than ${COMMENT_MAX_CHARACTER_LENGTH} characters long.`;
  }
};

export type CommentErrors = {
  comment: string | undefined;
  originalComment: string | null | undefined;
  moderationResult: string | null | undefined;
}

export async function editCommentAction(request: Request) {
  const user = await requireUser(request);
  const formData = await request.formData();
  const data = Object.fromEntries(formData);

  const comment = await getComment(data.id as string);

  let isNew = false;
  if (comment?.id == undefined) {
    isNew = true;
  }

  const canEdit = await checkIfEditable(user, isNew, comment?.active, comment?.createdByUserId);
  invariant(canEdit, 'Cannot edit comment');

  const isUpdate = data.update === "true";
  const isRemove = data.remove === "true";

  invariant(isNew || isUpdate || isRemove, "update or remove action is required");

  if (isUpdate) {
    let moderationText: string | null;
    moderationText = 'test';

    let formErrors: CommentErrors = {
      comment: validateComment(data.comment as string),
      originalComment: null,
      moderationResult: moderationText ? null : ''
    };

    if (Object.values(formErrors).some(Boolean)) {
      formErrors.originalComment = data.comment as string;
      return json(formErrors);
    }

    const moderationResults = await getModerationResults(data.comment as string);

    if (moderationResults.length) {
      formErrors.moderationResult = moderationResults.join(', ');
    }

    if (isNew) {
      const newComment = await createComment({
        memeSlug: data.memeSlug as string,
        text: data.comment as string,
        moderationResult: formErrors.moderationResult as string,
        moderationComment: formErrors.moderationResult == null 
            ? null 
            : comment?.moderationComment,
        active: formErrors.moderationResult == null,
        createdByUserId: user.id
      });

      return redirectDocument(`/memes/${newComment.memeSlug}?commentId=${newComment.id}`);
    }
    else {
      const updatedComment = await updateComment({
        id: data.id as string,
        text: data.comment as string,
        moderationResult: formErrors.moderationResult as string,
        moderationComment: formErrors.moderationResult == null 
            ? null 
            : comment?.moderationComment,
        active: formErrors.moderationResult == null,
        updatedByUserId: user.id
      });

      return redirectDocument(`/memes/${updatedComment.memeSlug}?commentId=${updatedComment.id}`);
    }
  }
  else if (isRemove) {
    invariant(comment, 'Comment not found');

    await removeComment(comment.id as string);

    return redirectDocument(`/memes/${comment.memeSlug}/comment/removed`);
  }
}