import { requireUser } from "~/session.server";
import { getTitle } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Meme Removed') }];

export const loader = async ({
  request,
}: LoaderFunctionArgs) => {
  await requireUser(request);

  return null;
};

export default function RemovedMeme() {
  return (
    <div className="container">
      <div className="row g-3">
        <div className="col-md-4">
          <div className="alert alert-primary" role="alert">
            The meme has been removed.
          </div>
          <p>
            <Link
              className="btn btn-primary"
              to="/"
            >
              Continue to the memes
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
}
