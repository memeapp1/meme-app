import { USER_INTERSTITIAL_MESSAGE_KEY, USER_CURRENT_PAGE_URL_KEY, getSession, requireUser, sessionStorage } from "~/session.server";
import { getTitle, safeRedirect } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Profile Updated') }];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  await requireUser(request);
  const session = await getSession(request);
  const interstitialMessage = session.get(USER_INTERSTITIAL_MESSAGE_KEY) || null;
  let currentPageUrl = session.get(USER_CURRENT_PAGE_URL_KEY) || null;
  currentPageUrl = safeRedirect(currentPageUrl)

  if (interstitialMessage == null) {
    return redirect('/profile');
  }

  return json(
    { interstitialMessage, currentPageUrl },
    {
      headers: {
        "Set-Cookie": await sessionStorage.commitSession(session)
      },
    }
  );
}

export default function ProfileUpdated() {
  const { interstitialMessage, currentPageUrl } = useLoaderData<typeof loader>();

  return (
    <div className="container">
      <div className="row g-3">
        <div className="col-md-4">
          <div className="alert alert-primary" role="alert">
            {interstitialMessage}
          </div>
          <p>
            <Link
              className="btn btn-primary"
              to={currentPageUrl}
            >
              Back to profile
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
}
