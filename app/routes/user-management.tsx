import invariant from "tiny-invariant";
import { addRoleToUser, getUsers, getRole, getRoles, getUserByEmail, removeRoleFromUser, getUserById } from "~/models/user.server";
import { requireUser } from "~/session.server";
import { MAX_LENGTH_FOR_INPUT, SimulateNetworkDelay, checkIfAdmin, getTitle } from "~/utils";
import { useDebounceSubmit } from "remix-utils/use-debounce-submit";
import { Pagination } from "~/components/pagination";

export const meta: MetaFunction = () => [{ title: getTitle('User Management') }];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);

  invariant(isAdmin, 'User must be an admin.');

  const url = new URL(request.url);
  let search = url.searchParams.get("search") as string;

  if (search == null) {
    search = '';
  }

  let page = Number(url.searchParams.get("page"));

  if (page == 0) {
    page = 1;
  }

  const { results, totalPageCount, totalResultsCount } = await getUsers(search, page);

  const roles = await getRoles();

  return json({ userId: user.id, roles, results, search, page, totalPageCount, totalResultsCount });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  let user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);

  invariant(isAdmin, 'User must be an admin.');

  const formData = await request.formData();
  const email = formData.get("email");
  const roleId = formData.get("roleId");
  const removeRole = formData.get("removeRole");

  invariant(roleId, 'Role is empty');

  const existingRole = await getRole(roleId as string);
  if (!existingRole) {
    return json(
      {
        errors: {
          role: "The role does not exist."
        },
      },
      { status: 400 },
    );
  }

  const existingUser = await getUserByEmail(email as string);
  if (!existingUser) {
    return json(
      {
        errors: {
          email: "The email does not exist."
        },
      },
      { status: 400 },
    );
  }

  if (removeRole == 'true') {
    await removeRoleFromUser(roleId as string, existingUser.id);
  }
  else {
    await addRoleToUser(roleId as string, existingUser.id, user.id);
  }

  return null;
};

export default function Settings() {
  const fetcher = useFetcher();
  const actionData = useActionData<typeof action>();
  const { userId, roles, results, search, page, totalPageCount, totalResultsCount } = useLoaderData<typeof loader>();

  const navigation = useNavigation();
  // https://sergiodxa.github.io/remix-utils/#md:debounced-fetcher-and-submit
  const submit = useDebounceSubmit();
  const searching =
    navigation.location &&
    new URLSearchParams(navigation.location.search).has(
      "search"
    );

  useEffect(() => {
    const searchField = document.getElementById("search");
    if (searchField instanceof HTMLInputElement) {
      searchField.value = search || "";
    }
  }, [search]);

  const isSubmitting = Boolean(
    navigation.state === "submitting"
  );

  const isRoleUpdate = navigation.formData
    ? navigation.formData.get("roleIdUpdate") === "true" && isSubmitting
    : false;

  return (
    <div className="container">
      <div className="row g-3">
        <div className="col-md-4">
          <div className="accordion">
            <div className="accordion-item">
              <h2 className="accordion-header">
                <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel1" aria-expanded="true" aria-controls="panel1">
                  Assign Role
                </button>
              </h2>
              <div id="panel1" className="accordion-collapse collapse show">
                <div className="accordion-body">

                  <Form method="post">
                    <div className="mb-3">
                      <label htmlFor="email" className="form-label">Email</label>
                      <input type="email" className="form-control" id="email" name="email" maxLength={MAX_LENGTH_FOR_INPUT} />
                      {actionData?.errors?.email ? (
                        <div className="custom-invalid-feedback">
                          {actionData.errors.email}
                        </div>
                      ) : null}
                    </div>
                    <div className="mb-3">
                      <label htmlFor="roleId" className="form-label">Role</label>
                      <select className="form-select" id="roleId" name="roleId">
                        {roles.map((role) => (
                          <option key={role.id} value={role.id}>{role.name}</option>
                        ))}
                      </select>
                      {actionData?.errors?.role ? (
                        <div className="custom-invalid-feedback">
                          {actionData.errors.role}
                        </div>
                      ) : null}
                    </div>
                    <input type="hidden" name="roleIdUpdate" value="true" />
                    <button type="submit" className="btn btn-primary" disabled={isRoleUpdate}>
                      {isRoleUpdate ? (
                        <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                        : null}
                      {isRoleUpdate ? 'Assigning Role...' : "Assign Role"}
                    </button>
                  </Form>

                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-md-8">

          <Form
            id="search-form"
            onChange={(event) => {
              const isFirstSearch = search === null;
              submit(event.currentTarget, {
                debounceTimeout: 1000,
                replace: !isFirstSearch,
                preventScrollReset: true,
              });
            }}
            role="search"
          >
            <div className="input-group mb-3">
              <input
                aria-label="Search users"
                className={searching ? "loading form-control search" : "form-control search"}
                defaultValue={search || ""}
                id="search"
                name="search"
                placeholder="Search"
                type="search"
              />
              <div
                hidden={!searching}
                className="spinner-border spinner-border-sm search-spinner"
                role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
              <button className="btn btn-success search" type="submit">Search</button>
              <Pagination search={search} useSearch={true} page={page} totalPageCount={totalPageCount} />
            </div>
          </Form>

          <strong>Total Users:</strong> {totalResultsCount}

          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>Email</th>
                <th>Username</th>
                <th>Roles</th>
              </tr>
            </thead>
            <tbody>
              {results.map((user) => (
                <tr key={user.id}>
                  <td>
                    <Link
                      to={`/profile?id=${user.id}`}>
                      {user.email}
                    </Link>
                  </td>
                  <td>
                    {user.username}
                  </td>
                  <td>
                    {user.roles.map((role) => (
                      <div key={role.role.id}>
                        {role.role.name}
                        {role.role.name != 'Admin' || user.id != userId ?
                          (
                            <fetcher.Form method="post">
                              <input type="hidden" name="email" value={user.email} />
                              <input type="hidden" name="roleId" value={role.role.id} />
                              <button
                                className="btn btn-danger btn-sm"
                                name="removeRole"
                                value="true"
                              >
                                Remove
                              </button>
                            </fetcher.Form>
                          ) : null}
                      </div>
                    ))}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>

          <Pagination search={search} useSearch={true} page={page} totalPageCount={totalPageCount} />
        </div>
      </div>
    </div>
  );
}

