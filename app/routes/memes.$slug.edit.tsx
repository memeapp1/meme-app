import { getMeme } from "~/models/meme.server";
import invariant from "tiny-invariant";
import { requireUser } from "~/session.server";
import { MemeErrors, editMemeAction } from "~/edit-meme.server";
import { EditMeme } from "~/components/edit-meme";
import { SimulateNetworkDelay, checkIfEditable, getTitle } from "~/utils";

export const meta: MetaFunction<typeof loader> = ({
  data
}) => {
  invariant(data, "data is required");
  return [{ title: getTitle(`Edit ${data.meme.topText} ${data.meme.bottomText}`) }];
};

export const loader = async ({
  params,
  request
}: LoaderFunctionArgs) => {
  const user = await requireUser(request);
  invariant(params.slug, "params.slug is required");

  const url = new URL(request.url);
  let search = url.searchParams.get("search") as string;

  if (search == null) {
    search = '';
  }

  let page = Number(url.searchParams.get("page"));

  if (page == 0) {
    page = 1;
  }

  const meme = await getMeme(params.slug, user.id);
  
  if (!meme) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  const canEdit = await checkIfEditable(user, false, meme?.active, meme?.createdByUserId);
  invariant(canEdit, 'Cannot edit meme');

  return json({ meme, search, page });
};

export const action = async ({
  request,
}: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  return await editMemeAction(request);
};

export default function EditMemeResponse() {
  const { meme, search, page } = useLoaderData<typeof loader>();
  const errors = useActionData<typeof action>();

  const memeErrors: MemeErrors = {
    slug: errors?.slug,
    memeType: errors?.memeType,
    topText: errors?.topText,
    bottomText: errors?.bottomText,
    moderationResult: errors?.moderationResult,
  };

  return (
    <EditMeme meme={{
      id: meme.id,
      slug: meme.slug,
      memeType: meme.memeType,
      topText: meme.topText,
      bottomText: meme.bottomText
    }}
      search={search}
      page={page}
      errors={memeErrors} />
  );
}
