import { Meme } from "@prisma/client";
import { MemeErrors, editMemeAction } from "~/edit-meme.server";
import { EditMeme } from "~/components/edit-meme";
import { SimulateNetworkDelay, convertTitleToSlug, getTitle } from "~/utils";
import memeSelections from '../../meme-selections.json';

export const meta: MetaFunction = () => [{ title: getTitle('New Meme') }];

export const loader = async ({
  params,
  request
}: LoaderFunctionArgs) => {

  const url = new URL(request.url);
  let search = url.searchParams.get("search") as string;

  if (search == null) {
    search = '';
  }

  let page = Number(url.searchParams.get("page"));

  if (page == 0) {
    page = 1;
  }

  const meme: Meme = {
    slug: "",
    memeType: convertTitleToSlug(memeSelections.filter(x => x.name.toUpperCase() == 'Chuck Norris Approves'.toUpperCase())[0]?.name)
      || convertTitleToSlug(memeSelections[0].name),
    topText: "",
    bottomText: "",
    active: false,
    createdByUserId: "",
    createdDate: new Date(),
    updatedByUserId: null,
    updatedDate: null
  };
  return json({ meme, search, page });
};

export const action = async ({
  request,
}: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  return await editMemeAction(request);
};

export default function NewMeme() {
  const { meme, search, page } = useLoaderData<typeof loader>();
  const errors = useActionData<typeof action>();

  const memeErrors: MemeErrors = {
    slug: errors?.slug,
    memeType: errors?.memeType,
    topText: errors?.topText,
    bottomText: errors?.bottomText,
    moderationResult: errors?.moderationResult,
  };

  return (
    <EditMeme meme={{
      slug: meme.slug,
      memeType: meme.memeType,
      topText: meme.topText,
      bottomText: meme.bottomText,
    }}
      search={search}
      page={page}
      errors={memeErrors} />
  );
}
