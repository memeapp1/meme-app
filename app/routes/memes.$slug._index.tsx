import invariant from "tiny-invariant";
import { CommentDisplay } from "~/components/comment-display";
import { CommentResult, getCommentPageNumber, getComments } from "~/models/comment.server";
import { getMeme, getSimilarMemes } from "~/models/meme.server";
import { getUser } from "~/session.server";
import { checkIfAdmin, checkIfEditable, checkIfPosted, getTitle } from "~/utils";
import { Pagination } from "~/components/pagination";
import { EditCommentButton } from "~/components/edit-comment-button";
import { MemeImage } from "~/components/meme-image";
import { getUserByIdUsername } from "~/models/user.server";

export const meta: MetaFunction<typeof loader> = ({
  data
}) => {
  invariant(data, "data is required");
  return [{ title: getTitle(`${data.meme.topText} ${data.meme.bottomText}`) }];
};

export const loader = async ({
  params,
  request
}: LoaderFunctionArgs) => {
  invariant(params.slug, "params.slug is required");

  const user = await getUser(request);
  const meme = await getMeme(params.slug, user?.id);

  if (!meme) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  const isPosted = await checkIfPosted(user ?? undefined, meme?.active, meme?.createdByUserId);

  if (!isPosted) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  const memeCreatorUsername = await getUserByIdUsername(meme.createdByUserId);

  const canEdit = await checkIfEditable(user ?? undefined, false, meme?.active, meme?.createdByUserId);
  const isAdmin = checkIfAdmin(user ?? undefined);

  const includeUnapproved = isAdmin;

  const url = new URL(request.url);

  const commentId = url.searchParams.get("commentId");
  if (commentId != null) {
    const commentPageNumber = await getCommentPageNumber(commentId, meme.slug, user?.id, includeUnapproved);
    if (commentPageNumber != undefined) {
      return redirectDocument(`/memes/${meme.slug}?commentsPage=${commentPageNumber}#${commentId}`);
    }
  }

  let commentsPage = Number(url.searchParams.get("commentsPage"));
  let similarMemesPage = Number(url.searchParams.get("similarMemesPage"));

  if (commentsPage == 0) {
    commentsPage = 1;
  }

  if (similarMemesPage == 0) {
    similarMemesPage = 1;
  }
  
  const { comments, commentsTotalPageCount, commentsTotalResultsCount } = await getComments(params.slug, user?.id, includeUnapproved, commentsPage);
  const { similarMemes, similarMemesTotalPageCount, similarMemesTotalResultsCount } = await getSimilarMemes(user?.id, meme.slug, meme.memeType, similarMemesPage);

  let newComment: CommentResult = {
    id: "",
    text: "",
    memeSlug: meme?.slug as string,
  };

  return json({ meme, canEdit, memeCreatorUsername, comments, commentsPage, commentsTotalPageCount, commentsTotalResultsCount, newComment, user, similarMemes, similarMemesPage, similarMemesTotalPageCount, similarMemesTotalResultsCount });
};

export default function ViewMeme() {
  const { meme, canEdit, memeCreatorUsername, comments, commentsPage, commentsTotalPageCount, commentsTotalResultsCount, newComment, user, similarMemes, similarMemesPage, similarMemesTotalPageCount, similarMemesTotalResultsCount } = useLoaderData<typeof loader>();

  const voteRedirectTo = `/memes/${meme.slug}`;

  return (
    <div className="container">
      {canEdit ? (
        <>
          <Link
            to={`/memes/${meme.slug}/edit`}
            className="btn btn-secondary"
          >
            Edit
          </Link>
          &nbsp;
          <Link
            to={`/memes/${meme.slug}/remove`}
            className="btn btn-danger"
          >
            Remove
          </Link>
        </>
      ) : null}

      <div className="row g-3">

        <div className="col-sm-6">
          <MemeImage key={meme.slug} meme={meme} user={user} voteRedirectTo={voteRedirectTo} />
          Created by: {memeCreatorUsername}
        </div>

        <div className="col-md-6">
          <strong>Total Comments:</strong> {commentsTotalResultsCount.toLocaleString()}
          <Pagination useSearch={false} page={commentsPage} pageValueName="commentsPage" totalPageCount={commentsTotalPageCount} />

          {comments.map((comment) => (
            <CommentDisplay key={comment.id} comment={comment} user={user} />
          ))}

          <Pagination useSearch={false} page={commentsPage} pageValueName="commentsPage" totalPageCount={commentsTotalPageCount} />

          <EditCommentButton comment={newComment} user={user} />
        </div>
        {similarMemes.length > 0 ? (
          <div className="col-md-12">
            <hr />
            <h3>
              Similar Memes ({similarMemesTotalResultsCount})
            </h3>

            <Pagination useSearch={false} page={similarMemesPage} pageValueName="similarMemesPage" totalPageCount={similarMemesTotalPageCount} />

            {similarMemes.length > 0 ? (
              <div className="container">
                <div className="row">
                  {similarMemes.map((item) => (
                    <div key={item.slug} className="col-sm-6">
                      <MemeImage meme={item} user={user} renderLink={true} voteRedirectTo={voteRedirectTo} />
                    </div>
                  ))}
                </div>
              </div>
            ) : (
              <div className="alert alert-secondary" role="alert">
                No memes found.
              </div>
            )}

            <br />
            <Pagination useSearch={false} page={similarMemesPage} pageValueName="similarMemesPage" totalPageCount={similarMemesTotalPageCount} />
          </div>
        ) : null}
      </div>
    </div>
  );
}

