import invariant from "tiny-invariant";
import { getComment } from "~/models/comment.server";
import { getMeme } from "~/models/meme.server";
import { requireUser } from "~/session.server";

import { checkIfEditable, getTitle } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Remove Comment') }];

export const loader = async ({
  params,
  request
}: LoaderFunctionArgs) => {
  const user = await requireUser(request);

  invariant(params.commentId, "params.commentId is required");

  const comment = await getComment(params.commentId);
  invariant(comment, `Comment not found: ${params.commentId}`);

  const meme = await getMeme(comment.memeSlug, user.id);
  
  if (!meme) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  const canEdit = await checkIfEditable(user, false, comment?.active, comment?.createdByUserId);
  invariant(canEdit, 'Cannot remove comment');

  return json({ comment, meme });
};

export default function RemoveComment() {
  const { comment, meme } = useLoaderData<typeof loader>();
  const navigation = useNavigation();
  const isSubmitting = Boolean(
    navigation.state === "submitting"
  );

  let isRemoveAction = navigation.formData
    ? navigation.formData?.get("remove") === "true" && isSubmitting
    : false;

  return (
    <div className="container">
      <div className="row g-3">
        <div className="col-md-5">
          <div className="alert alert-warning" role="alert">
            Are you sure you want to remove this comment?
            <br />
            <br />
            <p className="h6 pb-2 mb-4 text-secondary">
              {comment.text}
            </p>
          </div>
          <Form method="post"
            action={`/memes/comment/${comment.id}/edit`}>
            <Link
              to={`/memes/${meme.slug}?commentId=${comment.id}`}
              className="btn btn-secondary"
            >
              Back
            </Link>
            &nbsp;
            <input type="hidden" name="id" value={comment.id} />
            <button className="btn btn-danger" type="submit" name="remove" value="true" disabled={isRemoveAction}>
              {isRemoveAction ? (
                <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                : null}
              {isRemoveAction ? 'Removing Comment...' : "Remove Comment"}
            </button>
          </Form>
        </div>
      </div>
    </div>
  );
}
