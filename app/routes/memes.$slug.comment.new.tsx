import { Comment } from "@prisma/client";
import { SimulateNetworkDelay, getTitle } from "~/utils";
import { CommentErrors, editCommentAction } from "~/edit-comment.server";
import { EditComment } from "~/components/edit-comment";
import invariant from "tiny-invariant";
import { getMeme } from "~/models/meme.server";
import { requireUserId } from "~/session.server";

export const meta: MetaFunction = () => [{ title: getTitle('New Comment') }];

export const loader = async ({
  params,
  request
}: LoaderFunctionArgs) => {
  const userId = await requireUserId(request);
  invariant(params.slug, "params.slug is required");
  
  const meme = await getMeme(params.slug, userId);
  
  if (!meme) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  const comment: Comment = {
    id: "",
    text: "",    
    moderationResult: null,
    moderationComment: null,
    active: false,
    memeSlug: meme.memeSlug,
    createdByUserId: "",
    createdDate: new Date(),
    updatedByUserId: null,
    updatedDate: null    
  };

  return json({ comment, meme });
};

export const action = async ({
  request,
}: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  return await editCommentAction(request);
};

export default function NewComment() {
  const { comment, meme } = useLoaderData<typeof loader>();
  const errors = useActionData<typeof action>();

  const commentErrors: CommentErrors = {
    comment: errors?.comment,
    originalComment: errors?.originalComment,
    moderationResult: errors?.moderationResult,
  };

  return (
    <EditComment comment={{
      id: comment.id,
      text: comment.text,
      memeSlug: comment.memeSlug,
    }}
      meme={meme}
      errors={commentErrors} />
  );
}
