import invariant from "tiny-invariant";
import { getMeme } from "~/models/meme.server";
import { requireUserId } from "~/session.server";
import { getTitle } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Comment Removed') }];

export const loader = async ({
  params,
  request
}: LoaderFunctionArgs) => {
  const userId = await requireUserId(request);

  invariant(params.slug, "params.slug is required");

  const meme = await getMeme(params.slug, userId);
  
  if (!meme) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  return json({ meme });
};

export default function RemovedComment() {
  const { meme } = useLoaderData<typeof loader>();

  return (
    <div className="container">
      <div className="row g-3">
        <div className="col-md-4">
          <div className="alert alert-primary" role="alert">
            The comment has been removed.
          </div>
          <p>
            <Link
              className="btn btn-primary"
              to={`/memes/${meme.slug}`}
            >
              Back to the meme
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
}
