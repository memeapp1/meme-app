import { getModerationResults } from "./content-moderation";
import { getMeme, createMeme, updateMeme, getMemeById, removeMeme } from "./models/meme.server";
import { requireUser } from "./session.server";
import Sqids from "sqids";
import { BOTTOM_TEXT_MAX_CHARACTER_LENGTH, MIN_CHARACTER_LENGTH, TOP_TEXT_MAX_CHARACTER_LENGTH, checkIfEditable, convertTitleToSlug, generateApiMemeImageUrl, getRandomInt } from "./utils";
import invariant from "tiny-invariant";
import memeSelections from '../meme-selections.json';
import { generateImage } from "./routes/memes.$slug.image";

export const validateMemeType = (memeType: string) => {
  if (!memeType) {
    return "Meme Type is required.";
  }
  else if (memeSelections.filter(x => convertTitleToSlug(x.name) == convertTitleToSlug(memeType)).length == 0) {
    return `Meme Type is not valid.`;
  }
};

export const validateTopText = (topText: string) => {
  if (typeof topText !== "string" || topText.length === 0) {
    return "Top Text is required.";
  }
  else if (topText.length < MIN_CHARACTER_LENGTH) {
    return `Top Text must be at least ${MIN_CHARACTER_LENGTH} characters long.`;
  }
  else if (topText.length > TOP_TEXT_MAX_CHARACTER_LENGTH) {
    return `Top Text must be no more than ${TOP_TEXT_MAX_CHARACTER_LENGTH} characters long.`;
  }
};

export const validateBottomText = (bottomText: string) => {
  if (typeof bottomText !== "string" || bottomText.length === 0) {
    return "Bottom Text is required.";
  }
  else if (bottomText.length < MIN_CHARACTER_LENGTH) {
    return `Bottom Text must be at least ${MIN_CHARACTER_LENGTH} characters long.`;
  }
  else if (bottomText.length > BOTTOM_TEXT_MAX_CHARACTER_LENGTH) {
    return `Bottom Text must be no more than ${BOTTOM_TEXT_MAX_CHARACTER_LENGTH} characters long.`;
  }
};

export type MemeErrors = {
  memeType: string | undefined;
  topText: string | undefined;
  bottomText: string | undefined;
  moderationResult: string | null | undefined;
}

export async function editMemeAction(request: Request) {
  const user = await requireUser(request);
  const formData = await request.formData();
  const data = Object.fromEntries(formData);

  const meme = await getMemeById(data.id as string, user.id);

  let isNew = false;
  if (meme?.id == undefined) {
    isNew = true;
  }

  const canEdit = await checkIfEditable(user, isNew, meme?.active, meme?.createdByUserId);

  invariant(canEdit, 'Cannot edit meme');

  const isUpdate = data.update === "true";
  const isRemove = data.remove === "true";

  invariant(isNew || isUpdate || isRemove, "update or remove action is required");

  if (isUpdate) {
    let moderationText: string | null;
    moderationText = 'test';

    const formErrors: MemeErrors = {
      memeType: validateMemeType(data.memeType as string),
      topText: validateTopText(data.topText as string),
      bottomText: validateBottomText(data.bottomText as string),
      moderationResult: moderationText ? null : ''
    };

    if (Object.values(formErrors).some(Boolean)) {
      return json(formErrors);
    }

    const moderationResults = await getModerationResults(`${data.topText} ${data.bottomText}`);

    if (moderationResults.length) {
      formErrors.moderationResult = moderationResults.join(', ');
    }

    let slug: string;

    if (isNew) {
      const sqids = new Sqids();
      do {
        const id = sqids.encode([getRandomInt(1000000000)]);
        slug = id;
        const existingMeme = await getMeme(slug, user.id);
        if (existingMeme == null) {
          break;
        }
      } while (true);
    }
    else {
      slug = meme?.slug as string;
    }

    const isBrowserRender = data.imageData != null && data.imageData != '';
    if (!isBrowserRender) {
      const memeImageUrl = generateApiMemeImageUrl(data.memeType as string, data.topText as string, data.bottomText as string);
      try {
        const image = await generateImage(memeImageUrl);
        data.imageData = image.toString('base64');
      } catch (error) {
        console.log(error);
        data.imageData = '';
      }
    }

    if (isNew) {
      await createMeme({
        slug: slug,
        memeType: data.memeType as string,
        topText: data.topText as string,
        bottomText: data.bottomText as string,
        imageData: data.imageData as string,
        isBrowserRender: isBrowserRender,
        moderationResult: formErrors.moderationResult as string,
        moderationComment: formErrors.moderationResult == null
          ? null
          : meme?.moderationComment,
        active: formErrors.moderationResult == null,
        createdByUserId: user.id
      });

      return redirectDocument(`/memes/${slug}`);
    }
    else {
      await updateMeme(
        slug,
        {
          slug: slug,
          memeType: data.memeType as string,
          topText: data.topText as string,
          bottomText: data.bottomText as string,
          imageData: data.imageData as string,
          isBrowserRender: isBrowserRender,
          moderationResult: formErrors.moderationResult as string,
          moderationComment: formErrors.moderationResult == null
            ? null
            : meme?.moderationComment,
          active: formErrors.moderationResult == null,
          updatedByUserId: user.id
        }
      );

      return redirectDocument(`/memes/${slug}`);
    }
  }
  else if (isRemove) {
    invariant(meme, 'Meme not found');

    await removeMeme(meme.slug as string);

    return redirectDocument(`/memes/removed`);
  }
}