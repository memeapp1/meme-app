export type ModerationTab = {
  title?: string;
  link?: string;
};

const moderationTabs = {
  records: {} as Record<string, ModerationTab>,

  getAll(): ModerationTab[] {
    return Object.keys(moderationTabs.records)
      .map((key) => moderationTabs.records[key]);
  },

  async create(values: ModerationTab): Promise<ModerationTab> {
    const newLink = { ...values };
    moderationTabs.records[values.title?.toString() ?? ''] = values;
    return newLink;
  },
};

[
  {
    title: "Memes",
    link: "/memes/moderation/memes"
  },
  {
    title: "Comments",
    link: "/memes/moderation/comments"
  },
].forEach((item) => {
  moderationTabs.create({
    ...item
  });
});

export function ModerationTabs() {
  return (
    <ul className="nav nav-tabs">
        {moderationTabs.getAll().map((tab) => (
          <li className="nav-item" key={tab.title}>
            <NavLink
              className={({ isActive, isPending }) =>
                isActive
                  ? "nav-link active"
                  : isPending
                    ? "nav-link pending"
                    : "nav-link"
              }
              to={tab.link} end
            >
              {tab.title}
            </NavLink>
          </li>
        ))}
      </ul>
  );
}