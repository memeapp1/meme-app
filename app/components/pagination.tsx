import { getPaginationPageNumbers } from "~/utils";

export function Pagination({
  search,
  page,
  totalPageCount,
  useSearch,
  pageValueName
}: {
  search?: string;
  useSearch: boolean
  page: number;
  totalPageCount: number;
  pageValueName?: string;
}) {
  const pageNumbers = getPaginationPageNumbers(page, totalPageCount);
  const pageParameterStringValue = pageValueName ?? 'page';

  if (totalPageCount > 1) {
    return (
      <nav aria-label="...">
        <ul className="pagination justify-content-end">
          {page > 1 ? (
            <li className="page-item">
              <Link
                preventScrollReset
                to={useSearch
                  ? `?search=${search}&page=${page - 1}`
                  : `?${pageParameterStringValue}=${page - 1}`}
                className="page-link"
              >
                Previous
              </Link>
            </li>
          ) : (
            <li className="page-item disabled">
              <span className="page-link">Previous</span>
            </li>
          )}

          {pageNumbers.map((pageNumber) => (
            pageNumber == page ? (
              <li key={pageNumber} className="page-item active" aria-current="page">
                <span className="page-link">{page}</span>
              </li>
            ) : (
              <li key={pageNumber} className="page-item">
                <Link
                  preventScrollReset
                  to={useSearch
                    ? `?search=${search}&page=${pageNumber}`
                    : `?${pageParameterStringValue}=${pageNumber}`}
                  className="page-link"
                >
                  {pageNumber}
                </Link>
              </li>
            )
          ))}

          {totalPageCount > page ? (
            <li className="page-item">
              <Link
                preventScrollReset
                to={useSearch
                  ? `?search=${search}&page=${page + 1}`
                  : `?${pageParameterStringValue}=${page + 1}`}
                className="page-link"
              >
                Next
              </Link>
            </li>
          ) : (
            <li className="page-item disabled">
              <span className="page-link">Next</span>
            </li>
          )}
        </ul>
      </nav>
    );
  }
  else {
    return null;
  }
};