export function Footer() {
  return (
    <footer className="text-center">
      <div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik" target="_blank" rel="noopener noreferrer">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon" target="_blank" rel="noopener noreferrer">www.flaticon.com</a></div>
      <div>Images from <a href="https://imgflip.com" title="Imgflip" target="_blank" rel="noopener noreferrer">Imgflip</a> and <a href="https://apimeme.com" title="APIMeme" target="_blank" rel="noopener noreferrer">APIMeme</a></div>
      <div><a href="/creating-a-meme-app/index.html">Documentation</a></div>
      <div><a href="https://meme-app-demo.vercel.app" title="Vercel" rel="noopener noreferrer">Vercel</a> • <a href="https://meme-app.fly.dev" title="Fly" rel="noopener noreferrer">Fly.io</a> • <a href="https://meme-app-2etd.onrender.com" title="Render" rel="noopener noreferrer">Render</a></div>
    </footer>
  );
}