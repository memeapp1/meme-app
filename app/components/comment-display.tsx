import moment from "moment-timezone";
import { CommentResult } from "~/models/comment.server";
import { UserWithoutPassword } from "~/models/user.server";
import { EditCommentButton } from "./edit-comment-button";

export function CommentDisplay({
  comment,
  user
}: {
  comment: CommentResult;
  user: UserWithoutPassword;
}) {

  return (
    <div>
      <div className="card">
        <div className="card-body">
          <strong id={comment.id}>{comment.createdByUserName}</strong>

          <span className="clearfix">
            <span className="float-end">🕒 {moment.tz(comment.createdDate, "America/New_York").format('M/D/YYYY h:mm a')}</span>
          </span>

          {comment.active == false ?
            (
              comment.moderationComment != null ? (
                <div className="alert alert-danger" role="alert">
                  <strong>Moderation comment:</strong> {comment.moderationComment}
                  <br />The comment is not displayed publicly.
                </div>
              ) : (
                <div className="alert alert-warning" role="alert">
                  This comment is pending moderation and not displayed publicly.
                </div>
              )
            )
            : (<br />)}
          <p className="card-text muted" style={{ whiteSpace: 'pre-line' }}>{comment.text}</p>

          <EditCommentButton key={comment.id} comment={comment} user={user} />
        </div>
      </div>
    </div>
  );
}