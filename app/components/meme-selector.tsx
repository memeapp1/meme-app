import { convertTitleToSlug } from "~/utils";
import memeSelections from '../../meme-selections.json';

export function MemeSelector({
    memeType,
    modalId,
    onChange
}: {
    memeType: string;
    modalId: any;
    onChange: any;
}) {
    const modalTarget = `#${modalId}`;

    function onValueChange(value: string) {
        onChange(value);
    }

    return (
        <>
            <select className="form-select" name="memeType" id="memeType" defaultValue={memeType} required onChange={({ target: { value } }) => onValueChange(value)}>
                {memeSelections.map((meme) => (
                    <option key={convertTitleToSlug(meme.name)} value={convertTitleToSlug(meme.name)}>{meme.name}</option>
                ))}
            </select>

            <button type="button" className="btn btn-secondary btn-sm hide-noscript" data-bs-toggle="modal" data-bs-target={modalTarget}>
                Select Meme
            </button>
        </>
    );
}