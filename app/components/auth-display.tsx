import { MAX_LENGTH_FOR_INPUT } from "~/utils";

export type AuthErrors = {
  email: string;
  password: boolean;
};

export function AuthDisplay({
  action,
  actionData,
  enableSignIn,
  enableSignUp
}: {
  action: string;
  actionData: any;
  enableSignIn: boolean;
  enableSignUp: boolean;
}) {
  const navigation = useNavigation();
  const isSubmitting = Boolean(
    navigation.state === "submitting"
  );

  const [searchParams] = useSearchParams();
  const redirectTo = searchParams.get("redirectTo") ?? undefined;
  const emailRef = useRef<HTMLInputElement>(null);
  const passwordRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (actionData?.errors?.email) {
      emailRef.current?.focus();
    }
    else if (actionData?.errors?.password) {
      passwordRef.current?.focus();
    }
  }, [actionData]);

  return (
    <div className="container">
      {action == 'signin' ? (
        <h2>Sign In</h2>
      ) : (
        <h2>Sign Up</h2>
      )}
      <div className="row g-3">
        <div className="col-md-4">
          <Form method="post">
            <div className="mb-3">
              <label htmlFor="email" className="form-label">Email</label>
              <input
                ref={emailRef}
                id="email"
                required
                // eslint-disable-next-line jsx-a11y/no-autofocus
                autoFocus={true}
                name="email"
                type="email"
                autoComplete="email"
                aria-invalid={actionData?.errors?.email ? true : undefined}
                aria-describedby="email-error"
                className="form-control has-validation"
                maxLength={MAX_LENGTH_FOR_INPUT}
              />
              {actionData?.errors?.email ? (
                <div id="email-error" className="custom-invalid-feedback">
                  {actionData.errors.email}
                </div>
              ) : null}
            </div>
            <div className="mb-3">
              <label htmlFor="password" className="form-label">Password</label>
              <input
                id="password"
                ref={passwordRef}
                name="password"
                type="password"
                autoComplete="new-password"
                aria-invalid={actionData?.errors?.password ? true : undefined}
                aria-describedby="password-error"
                className="form-control has-validation"
              />
              {actionData?.errors?.password ? (
                <div id="password-error" className="custom-invalid-feedback">
                  {actionData.errors.password}
                </div>
              ) : null}
            </div>

            {action == 'signin' ? (
              <div className="mb-3">
                <div className="mb-3 form-check">
                  <input type="checkbox" className="form-check-input" id="remember" name="remember" value="on" />
                  <label className="form-check-label" htmlFor="remember">Remember me</label>
                </div>
              </div>
            ) : null}

            <input type="hidden" name="redirectTo" value={redirectTo} />
            <div className="mb-3">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={isSubmitting}
              >
                {isSubmitting ? (
                  <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                  : null}
                {isSubmitting ? (
                  <>{action == 'signup' ? <>Creating Account...</> : <>Signing In...</>}</>
                ) : (
                  <>{action == 'signup' ? <>Create Account</> : <>Sign In</>}</>
                )}
              </button>
            </div>

            {action == 'signup' && enableSignIn ? (
              <div className="mb-3">
                Already have an account?
                <Link
                  className="btn btn-link"
                  to={{
                    pathname: "/signin",
                    search: searchParams.toString(),
                  }}
                >
                  Sign in
                </Link>
              </div>
            ) : null}

            {action == 'signin' && enableSignUp ? (
              <div className="mb-3">
                Don't have an account?{" "}
                <Link
                  className="btn btn-link"
                  to={{
                    pathname: "/signup",
                    search: searchParams.toString(),
                  }}
                >
                  Sign up
                </Link>
              </div>
            ) : null}
          </Form>
        </div>
      </div>
    </div>
  );
};