import { MemeWithUserVote } from "~/models/meme.server";
import { UserWithoutPassword } from "~/models/user.server";

export function VoteButton({
  meme,
  user,
  redirectTo
}: {
  meme: Pick<MemeWithUserVote, "slug" | "voted" | "voteCount">;
  user: UserWithoutPassword;
  redirectTo: string;
}) {
  const fetcher = useFetcher();

  let vote = fetcher.formData
    ? fetcher.formData.get("vote") === "true"
    : meme.voted;

  if (user?.id == null) {
    vote = true;
  }

  const slug = meme.slug;

  const [isUsingJavascript, setIsUsingJavascript] = useState(false);

  useEffect(() => {
    setIsUsingJavascript(true);
  });

  const isDisabled = user?.id == null;

  return (
    <fetcher.Form method="post" action="/vote">
      <button
        disabled={isDisabled}
        aria-label={
          vote
            ? "Remove vote"
            : "Add vote"
        }
        title={
          vote
            ? "Remove vote"
            : "Add vote"
        }
        name="vote"
        className="btn btn-primary position-relative vote-button"
        value={vote ? "false" : "true"}
      >
        {vote ? "★" : "☆"}
        <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill text-bg-secondary">
          {meme.voteCount}
          <span className="visually-hidden">votes</span>
        </span>
      </button>
      <input type="hidden" name="slug" value={slug} />
      <input type="hidden" name="redirectTo" value={redirectTo} />
      <input type="hidden" name="isUsingJavascript" value={isUsingJavascript.toString()} />
    </fetcher.Form>
  );
};