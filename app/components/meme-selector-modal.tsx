import { convertTitleToSlug } from "~/utils";
import memeSelections from '../../meme-selections.json';
import { Pagination } from "./pagination";
import { useDebounceSubmit } from "remix-utils/use-debounce-submit";
import { RenderImage } from "./render-image";

export function MemeSelectorModal({
    modalId,
    search,
    page,
    topText,
    bottomText,
    onChange
}: {
    modalId: string;
    search: string;
    page: number;
    topText: string;
    bottomText: string;
    onChange: any;
}) {
    const navigation = useNavigation();

    function onValueChange(value: string) {
        onChange(value);
    }

    // https://sergiodxa.github.io/remix-utils/#md:debounced-fetcher-and-submit
    const submit = useDebounceSubmit();
    const searching =
        navigation.location &&
        new URLSearchParams(navigation.location.search).has(
            "search"
        );

    useEffect(() => {
        const searchField = document.getElementById("search");
        if (searchField instanceof HTMLInputElement) {
            searchField.value = search || "";
        }
    }, [search]);

    const memeRecordsPerPage = 8;

    const skip = (page - 1) * memeRecordsPerPage;
    const take = memeRecordsPerPage;

    const filteredMemeSelections = memeSelections
        .filter(x => search == '' || x.name.toUpperCase().indexOf(search.toUpperCase()) > -1)
        .sort((a, b) => { // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
            const nameA = a.name.toUpperCase(); // ignore upper and lowercase
            const nameB = b.name.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }

            // names must be equal
            return 0;
        });

    const totalResultsCount = filteredMemeSelections.length;

    const totalPageCount = Math.ceil(totalResultsCount / memeRecordsPerPage);

    const pagedMemeSelections = filteredMemeSelections.slice(skip, skip + take);

    return (
        <div className="modal fade" id={modalId} aria-labelledby={`${modalId}-label`} aria-hidden="true">
            <div className="modal-dialog modal-xl">
                <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id={`${modalId}-label`}>Select Meme</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">

                        <strong>Total Memes:</strong> {totalResultsCount.toLocaleString("en-US")}

                        <Form
                            id="search-form"
                            onChange={(event) => {
                                const isFirstSearch = search === null;
                                submit(event.currentTarget, {
                                    debounceTimeout: 1000,
                                    replace: !isFirstSearch,
                                    preventScrollReset: true,
                                });
                            }}
                            role="search"
                        >
                            <div className="input-group mb-3">
                                <input
                                    aria-label="Search memes"
                                    className={searching ? "loading form-control search" : "form-control search"}
                                    defaultValue={search || ""}
                                    id="search"
                                    name="search"
                                    placeholder="Search"
                                    type="search"
                                />
                                <div
                                    hidden={!searching}
                                    className="spinner-border spinner-border-sm search-spinner"
                                    role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                                <Pagination search={search} useSearch={true} page={page} totalPageCount={totalPageCount} />
                            </div>

                            {pagedMemeSelections.length > 0 ? (
                                <div className="container">
                                    <div className="row">
                                        {pagedMemeSelections.map((meme) => (
                                            <div key={`modalOption-${convertTitleToSlug(meme.name)}`} className="col-sm-3">
                                                <div className="card text-center">
                                                    <a className="nolinkunderline" href="#" onClick={() => onValueChange(convertTitleToSlug(meme.name))} data-bs-dismiss="modal">
                                                        <RenderImage memeType={meme.name} topText={topText} bottomText={bottomText} saveImage={false} />
                                                        <div className="card-body">
                                                            {meme.name}
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            ) : (
                                <div className="alert alert-secondary" role="alert">
                                    No memes found.
                                </div>
                            )}

                            <Pagination search={search} useSearch={true} page={page} totalPageCount={totalPageCount} />
                        </Form>

                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    );
}