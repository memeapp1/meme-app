import invariant from "tiny-invariant";
import type { User } from "~/models/user.server";
import { getUserById } from "~/models/user.server";

invariant(process.env.SESSION_SECRET, "SESSION_SECRET must be set");

export const sessionStorage = createCookieSessionStorage({
  cookie: {
    name: "__session",
    httpOnly: true,
    path: "/",
    sameSite: "lax",
    secrets: [process.env.SESSION_SECRET],
    secure: process.env.NODE_ENV === "production",
  },
});

const USER_SESSION_KEY = "userId";
export const USER_NEW_SIGNUP_KEY = "newSignUp";
export const USER_INTERSTITIAL_MESSAGE_KEY = "interstitialMessage";
export const USER_CURRENT_PAGE_URL_KEY = "currentPageUrl";

export async function getSession(request: Request) {
  const cookie = request.headers.get("Cookie");
  return sessionStorage.getSession(cookie);
}

export async function getUserId(
  request: Request,
): Promise<User["id"] | undefined> {
  const session = await getSession(request);
  const userId = session.get(USER_SESSION_KEY);
  return userId;
}

export async function getUser(request: Request) {
  const userId = await getUserId(request);
  if (userId === undefined) return null;

  const user = await getUserById(userId);
  if (user) return user;

  throw await signout(request);
}

export async function requireUserId(
  request: Request,
  redirectTo: string = new URL(request.url).pathname,
) {
  const userId = await getUserId(request);
  if (!userId) {
    const searchParams = new URLSearchParams([["redirectTo", redirectTo]]);
    throw redirect(`/signin?${searchParams}`);
  }
  return userId;
}

export async function requireUser(request: Request) {
  const userId = await requireUserId(request);

  const user = await getUserById(userId);
  if (user) return user;

  throw await signout(request);
}

export async function createUserSession({
  request,
  userId,
  remember,
  redirectTo,
  isNewSignUp,
}: {
  request: Request;
  userId: string;
  remember: boolean;
  redirectTo: string;
  isNewSignUp: boolean;
}) {
  const session = await getSession(request);
  session.set(USER_SESSION_KEY, userId);

  if (isNewSignUp) {
    setNewSignUp(session);
  }

  return redirect(redirectTo, {
    headers: {
      "Set-Cookie": await sessionStorage.commitSession(session, {
        maxAge: remember
          ? 60 * 60 * 24 * 7 // 7 days
          : undefined,
      }),
    },
  });
}

export async function signout(request: Request) {
  const session = await getSession(request);
  return redirect("/", {
    headers: {
      "Set-Cookie": await sessionStorage.destroySession(session),
    },
  });
}

export function setNewSignUp(session: Session) {
  session.flash(USER_NEW_SIGNUP_KEY, true);
}

export async function commitSession(session: Session) {
  return sessionStorage.commitSession(session);
}

export function setInterstitialMessage(session: Session, message: string, currentPageUrl: string) {
  session.flash(USER_INTERSTITIAL_MESSAGE_KEY, message);
  session.flash(USER_CURRENT_PAGE_URL_KEY, currentPageUrl);
}