import { PrismaClient } from "@prisma/client";
import bcrypt from "bcryptjs";
import invariant from "tiny-invariant";
import { faker } from '@faker-js/faker';
import memeSelections from '../meme-selections.json';
import { convertTitleToSlug } from "~/utils";

const prisma = new PrismaClient();

async function seed() {
  const currentDate = new Date();
  const email = "admin@meme-app.com";
  const username = "admin";

  const passwordHash = await bcrypt.hash(process.env.DEFAULT_PASSWORD as string, 10);

  let users = [
    {
      email,
      username,
      passwordHash: passwordHash,
      createdByUserId: '0'
    },
  ];

  for (let i = 0; i < 100; i++) {
    users.push({
      email: 'email',
      username: 'username',
      passwordHash: passwordHash,
      createdByUserId: '0'
    });
  }

  let i = 0;
  for (let user of users) {
    if (user.email == 'email') {
      user.username = faker.internet.userName() + '-' + i++;
      user.email = faker.internet.exampleEmail({firstName: user.username});      
    }

    console.log(`Initializing user: ${user.username}`);
  }

  console.log(`Inserting users...`);
  await prisma.user.createMany({
    data: users
  });

  const dbUsers = await prisma.user.findMany();

  function getUser(email: string) {
    const user = dbUsers.filter(x => x.email === email)[0];

    return user;
  }

  const user = getUser(email);
  invariant(user, 'User not found');

  const roles = [
    {
      id: '1',
      name: 'Admin',
      description: 'Admin user role',
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    {
      id: '2',
      name: 'Support',
      description: 'Support user role',
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    {
      id: '3',
      name: 'QA',
      description: 'QA user role',
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
  ];

  console.log(`Inserting roles...`);
  await prisma.role.createMany({
    data: roles
  });

  const adminUserRole = {
    id: '1',
    roleId: '1',
    userId: user.id,
    createdByUserId: user.id,
    createdDate: currentDate,
    updatedByUserId: null,
    updatedDate: null
  };

  const userRole = await prisma.userRole.upsert({
    where: {
      id: adminUserRole.id
    },
    update: adminUserRole,
    create: adminUserRole
  });

  const memes = [
    // https://www.pinatafarm.com/memes?template=the-bobs-meme
    {
      slug: "custom-the-bobs",
      memeType: "the-bobs",
      topText: "BESIDES MAKING MEMES",
      bottomText: "WHAT WOULD YOU SAY YOU DO HERE?",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    // https://www.pinatafarm.com/memes?template=1st-world-canadian-problems-meme
    {
      slug: "custom-1st-world-canadian-problems",
      memeType: "1st-world-canadian-problems",
      topText: "I SAID THANKS",
      bottomText: "THEY DIDN'T SAY YOU'RE WELCOME",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    // https://www.pinatafarm.com/memes?template=1990s-first-world-problems-meme
    {
      slug: "custom-1990s-first-world-problems",
      memeType: "1990s-first-world-problems",
      topText: "WHEN YOU REALIZE",
      bottomText: "STOCKS CAN GO DOWN",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    // https://www.pinatafarm.com/memes?template=britney-spears-meme
    {
      slug: "custom-britney-spears",
      memeType: "britney-spears",
      topText: "SOMETIMES...",
      bottomText: "I THINK ABOUT ALGEBRA",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    // https://www.pinatafarm.com/memes?template=chuck-norris-laughing-meme
    {
      slug: "custom-chuck-norris-laughing",
      memeType: "chuck-norris-laughing",
      topText: "CHUCK NORRIS",
      bottomText: "CAN MAKE A JOKE LAUGH",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    // https://www.pinatafarm.com/memes?template=bart-simpson-peeking-meme
    {
      slug: "custom-bart-simpson-peeking",
      memeType: "bart-simpson-peeking",
      topText: "JUST A REMINDER...",
      bottomText: "UR COOL :)",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    // https://www.pinatafarm.com/memes?template=grandma-finds-the-internet-meme
    {
      slug: "custom-grandma-finds-the-internet",
      memeType: "grandma-finds-the-internet",
      topText: "WHAT CHANNEL IS",
      bottomText: "THE NETFLIX",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    // https://www.pinatafarm.com/memes?template=ill-have-you-know-spongebob-meme
    {
      slug: "custom-ill-have-you-know-spongebob",
      memeType: "ill-have-you-know-spongebob",
      topText: "THAT'S COOL BUT...",
      bottomText: "   ",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    // https://www.pinatafarm.com/memes?template=skeptical-swardson-meme
    {
      slug: "custom-skeptical-swardson",
      memeType: "skeptical-swardson",
      topText: "WHAT MAGICAL TREAT HAS ENTERED MY HAND!?",
      bottomText: "ARE WE IN NARNIA!?",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
    // https://www.pinatafarm.com/memes?template=gangnam-style-psy-meme
    {
      slug: "custom-gangnam-style-psy",
      memeType: "gangnam-style-psy",
      topText: "   ",
      bottomText: "LIKE A BOSS",
      active: true,
      createdByUserId: user.id,
      createdDate: currentDate,
      updatedByUserId: null,
      updatedDate: null
    },
  ];

  for (let i = 0; i < 100; i++) {

    console.log(`Initializing meme: sample-meme-${i + 1}`);

    const meme = {
      slug: `sample-meme-${i + 1}`,
      memeType: convertTitleToSlug(memeSelections[Math.floor(Math.random() * memeSelections.length)].name),
      topText: faker.company.buzzPhrase(),
      bottomText: faker.company.catchPhrase(),
      active: true,
      createdByUserId: (await getUser(users[Math.floor(Math.random() * users.length)].email))?.id as string,
      createdDate: faker.date.recent({ days: 10 }),
      updatedByUserId: null,
      updatedDate: null
    };

    memes.push(meme);
  }

  const memesCopy = JSON.parse(JSON.stringify(memes));

  for (const meme of memesCopy) {

    const relatedMemeCountToCreate = Math.floor(Math.random() * 25);
    for (let x = 0; x < relatedMemeCountToCreate; x++) {
      console.log(`Initializing related meme: ${meme.slug}-related-${x + 1}`);
      const relatedMeme = {
        slug: `${meme.slug}-related-${x + 1}`,
        memeType: meme.memeType,
        topText: faker.company.buzzPhrase(),
        bottomText: faker.company.catchPhrase(),
        active: true,
        createdByUserId: (await getUser(users[Math.floor(Math.random() * users.length)].email))?.id as string,
        createdDate: faker.date.recent({ days: 10 }),
        updatedByUserId: null,
        updatedDate: null
      };

      memes.push(relatedMeme);
    }
  }

  // Source: https://www.phind.com/
  // Prompt: Generate a list of comments about funny memes in the JSON format: [{ text: "Comment here" }]
  const comments = [
    { "text": "This meme cracked me up" },
    { "text": "So relatable, haha." },
    { "text": "I'm still laughing at this one." },
    { "text": "Perfect timing on this meme." },
    { "text": "Made my day, thanks for sharing." },
    { "text": "This is the funniest thing I've seen today." },
    { "text": "Can't believe how true this is." },
    { "text": "Hilarious take on everyday life." },
    { "text": "This meme hits the spot." },
    { "text": "Laughed so hard, I almost cried." },
    { "text": "Such a clever play on words." },
    { "text": "I needed this laugh today." },
    { "text": "This meme is gold." },
    { "text": "So funny, I shared it with everyone." },
    { "text": "It's all about the delivery with this one." },
    { "text": "This made my day better." },
    { "text": "The internet never fails to amuse me." },
    { "text": "This meme is a mood." },
    { "text": "I love how creative this is." },
    { "text": "This had me rolling on the floor." },
    { "text": "Just what I needed to lighten up." },
    { "text": "This meme is a classic." },
    { "text": "So funny, I don't know why." },
    { "text": "This is the best meme I've seen in ages." },
    { "text": "This meme is a keeper." },
    { "text": "I'm still trying to figure out why it's so funny." },
    { "text": "This meme is a masterpiece." },
    { "text": "I'll be thinking about this one all day." },
    { "text": "This meme just gets it." },
    { "text": "This is the funniest thing I've seen in a long time." },
    { "text": "This meme is a work of art." },
    { "text": "I'm going to remember this forever." },
    { "text": "This meme is a game-changer." },
    { "text": "This is the kind of humor that brightens my day." },
    { "text": "This meme is a must-share." },
    { "text": "I'm still laughing at this meme." },
    { "text": "This meme is a hit." },
    { "text": "This meme is a treasure trove of laughter." },
    { "text": "I'm going to show this to everyone I know." },
    { "text": "This meme is a riot." },
    { "text": "This is the kind of content that makes my day." },
    { "text": "This meme is a gem." },
    { "text": "I'm still smiling from this meme." },
    { "text": "This meme is a standout." },
    { "text": "This is the funniest thing I've seen in a while." },
    { "text": "I'm going to keep this one saved." },
    { "text": "This meme is a winner." },
    { "text": "This is the kind of humor that never gets old." },
    { "text": "This meme is a must-see." },
    { "text": "I'm still chuckling over this one." },
    { "text": "I'm going to remember this one." },
  ];

  console.log(`Inserting memes...`);
  await prisma.meme.createMany({
    data: memes
  });

  const dbMemes = await prisma.meme.findMany();

  let commentsToAdd: any[] = [];
  let votesToAdd: any[] = [];

  let memeCount = 0;
  for (const meme of dbMemes) {
    console.log(`Initializing comments and votes for meme: ${meme.slug}`);

    const commentCountToCreate = Math.floor(Math.random() * comments.length);
    for (let i = 0; i < commentCountToCreate; i++) {
      commentsToAdd.push({
        text: comments[Math.floor(Math.random() * comments.length)].text,
        active: true,
        memeSlug: meme.slug,
        createdByUserId: (await getUser(users[Math.floor(Math.random() * users.length)].email))?.id as string,
        createdDate: faker.date.recent({ days: 10 }),
        updatedByUserId: null,
        updatedDate: null
      });
    }

    if (meme.slug.startsWith('custom-')
      && meme.slug.indexOf('-related-') == -1) {
      const voteCountToCreate = 25 - memeCount;
      for (let i = 0; i < voteCountToCreate; i++) {
        votesToAdd.push({
          memeSlug: meme.slug,
          createdByUserId: (await getUser(users[Math.floor(Math.random() * users.length)].email))?.id as string,
          createdDate: faker.date.recent({ days: 10 }),
          updatedByUserId: null,
          updatedDate: null
        });
      }
    }

    memeCount++;
  }

  console.log(`Inserting comments...`);
  await prisma.comment.createMany({
    data: commentsToAdd
  });

  console.log(`Inserting votes...`);
  await prisma.vote.createMany({
    data: votesToAdd
  });

  console.log(`Database has been seeded. 🌱`);
}

seed()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
